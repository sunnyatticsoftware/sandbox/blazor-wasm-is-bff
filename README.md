# blazor-wasm-is-bff

Run the application as it is, by launching together Sample.Auth (IdentityServer) and Sample.Server (Blazor WASM hosted server)

When it's configured with https, everything works well.

When it's configured with http, the redirection and the retrieval of cookie/token does not work, probably due to SameSite policy and other things.

## Run as HTTPS
Run both projects using the https profile with the following `appsettings.json` in `Sample.Auth`
```
{
  "IsHttpsEnabled": true,
  "IdentityServer": {
    "ApiScopes": [
      {
        "Name": "sample-api",
        "DisplayName": "Sample API"
      }
    ],
    "Clients": [
      {
        "ClientId": "sample",
        "ClientSecrets": [
          {
            "Value": "sample-secret",
            "Description": "The secret for Blazor WASM app"
          }
        ],
        "AllowedGrantTypes": ["authorization_code"],
        "AllowOfflineAccess": true,
        "AllowedScopes": ["openid", "profile", "sample-api"],
        "RedirectUris": [
          "https://localhost:5020/signin-oidc",
          "http://localhost:5021/signin-oidc"
        ],
        "PostLogoutRedirectUris ": [
          "https://localhost:5020/signout-callback-oidc",
          "http://localhost:5021/signout-callback-oidc"
        ]
      }
    ]
  }
}
```

and the following `appsettings.json` in `Sample.Auth`
```
{
  "IsHttpsEnabled": true,
  "Oidc": {
    "Authority": "https://localhost:5000",
    "ClientId": "sample",
    "ClientSecret": "sample-secret",
    "Scopes": ["openid", "profile", "sample-api", "offline_access"]
  }
}
```

Notice that in both, `IsHttpsEnabled` is set to `true` and the `Oidc__Authority` is set to the Identity Server's https url.

The following will be launched:
- `IdentityServer HTTPS` launch setting profile runs the `Sample.Auth` project, it runs on https://localhost:5000
- `Blazor HTTPS` launch setting profile runs the `Sample.Server` project, it runs on https://localhost:5020

## Run as HTTP
Run both projects using the https profile making the following changes

```
{
  "IsHttpsEnabled": false,
  "IdentityServer": {
    "ApiScopes": [
      {
        "Name": "sample-api",
        "DisplayName": "Sample API"
      }
    ],
    "Clients": [
      {
        "ClientId": "sample",
        "ClientSecrets": [
          {
            "Value": "sample-secret",
            "Description": "The secret for Blazor WASM app"
          }
        ],
        "AllowedGrantTypes": ["authorization_code"],
        "AllowOfflineAccess": true,
        "AllowedScopes": ["openid", "profile", "sample-api"],
        "RedirectUris": [
          "https://localhost:5020/signin-oidc",
          "http://localhost:5021/signin-oidc"
        ],
        "PostLogoutRedirectUris ": [
          "https://localhost:5020/signout-callback-oidc",
          "http://localhost:5021/signout-callback-oidc"
        ]
      }
    ]
  }
}
```

and the following `appsettings.json` in `Sample.Auth`
```
{
  "IsHttpsEnabled": false,
  "Oidc": {
    "Authority": "http://localhost:5001",
    "ClientId": "sample",
    "ClientSecret": "sample-secret",
    "Scopes": ["openid", "profile", "sample-api", "offline_access"]
  }
}
```

Notice that in both, `IsHttpsEnabled` is set to `false` and the `Oidc__Authority` is set to the Identity Server's http url.

The following will be launched:
- `IdentityServer` launch setting profile runs the `Sample.Auth` project, it runs on http://localhost:5001
- `Blazor` launch setting profile runs the `Sample.Server` project, it runs on http://localhost:5021