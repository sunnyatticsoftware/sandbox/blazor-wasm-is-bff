﻿using Duende.IdentityServer.Models;

namespace Sample.Auth;

public static class Config
{
	public static IEnumerable<IdentityResource> IdentityResources =>
		new IdentityResource[]
		{
			new IdentityResources.OpenId(),
			new IdentityResources.Profile()
		};
}