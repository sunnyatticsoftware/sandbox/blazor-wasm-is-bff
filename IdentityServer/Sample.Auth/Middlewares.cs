namespace Sample.Auth;

public static class Middlewares
{
	public static WebApplication UseOpenApi(this WebApplication app, string assemblyPath)
	{
		var openApiRelativePath = $"{assemblyPath}/swagger/v1/swagger.json";
		var openApiName = "Sample Auth";

		app.UseSwagger();
		app.UseSwaggerUI(options => options.SwaggerEndpoint(openApiRelativePath, openApiName));

		return app;
	}

	public static WebApplication UseHealthEndpoint(this WebApplication app)
	{
		app.MapGet("/health", () =>
		{
			var environmentName = app.Environment.EnvironmentName;
			var currentUtcDateTime = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
			var assemblyName = typeof(Program).Assembly.GetName();
			var result = $"Assembly={assemblyName}, Environment={environmentName}, CurrentUtcTime={currentUtcDateTime}";
			return Results.Ok(result);
		});

		return app;
	}
}