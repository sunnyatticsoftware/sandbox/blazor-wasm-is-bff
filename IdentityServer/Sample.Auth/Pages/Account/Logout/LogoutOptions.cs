
namespace Sample.Auth.Pages.Account.Logout;

public class LogoutOptions
{
    public static bool ShowLogoutPrompt = true;
    public static bool AutomaticRedirectAfterSignOut = false;
}