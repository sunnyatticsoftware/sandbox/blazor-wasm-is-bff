﻿using Sample.Auth;
using Serilog;
using Serilog.Events;

var serilogLogger =
	new LoggerConfiguration()
		.MinimumLevel.Debug()
		.MinimumLevel.Override("Microsoft", LogEventLevel.Information)
		.MinimumLevel.Override("Microsoft.AspNetCore.Hosting.Diagnostics", LogEventLevel.Warning)
		.MinimumLevel.Override("Microsoft.Hosting.Lifetime", LogEventLevel.Information)
		.MinimumLevel.Override("Microsoft.AspNetCore.Routing", LogEventLevel.Warning)
		.MinimumLevel.Override("Microsoft.AspNetCore.Authentication", LogEventLevel.Debug)
		.MinimumLevel.Override("System", LogEventLevel.Warning)
		.Enrich.FromLogContext()
		.WriteTo.Console(
			outputTemplate:
			"[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj} <s:{SourceContext}>{NewLine}{Exception}"
		)
		.CreateLogger();

Log.Logger = serilogLogger;

var builder = WebApplication.CreateBuilder(args);
builder.Host.UseSerilog();

var configuration = builder.Configuration;
var isHttpsEnabled = configuration.GetValue<bool>("IsHttpsEnabled");

var services = builder.Services;

services
	.AddEndpointsApiExplorer()
	.AddTokenProvider(configuration)
	.AddOpenApi();

var assemblyPath = $"/{typeof(Sample.Auth.Program).Assembly.GetName().Name!.ToLowerInvariant()}";

var app = builder.Build();

app.UseSerilogRequestLogging();

app.UsePathBase(assemblyPath);

if (app.Environment.IsDevelopment())
{
	app.UseDeveloperExceptionPage();
}

if (isHttpsEnabled)
{
	app.UseHttpsRedirection();
}

// UI
app.UseStaticFiles();
app.UseRouting();

app.UseOpenApi(assemblyPath);
app.UseIdentityServer();

// UI
app.UseAuthorization();
app.MapRazorPages().RequireAuthorization();

app.UseHealthEndpoint();
app.Run();

// ReSharper disable once PartialTypeWithSinglePart
namespace Sample.Auth
{
	public partial class Program
	{
	}
}