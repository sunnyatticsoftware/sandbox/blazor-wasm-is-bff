using Duende.IdentityServer.Models;
using Sample.Auth.Pages;

namespace Sample.Auth;

internal static class Registrations
{
	public static IServiceCollection AddTokenProvider(this IServiceCollection services, IConfiguration configuration)
	{
		// UI
		services.AddRazorPages();

		var clients = configuration.GetSection("IdentityServer:Clients").Get<List<Client>>()!.ToList();
		var secrets = clients.SelectMany(client => client.ClientSecrets);
		foreach (var secret in secrets)
		{
			secret.Value = secret.Value.Sha256();
		}
		
		var apiScopesFromConfig = configuration.GetSection("IdentityServer:ApiScopes").Get<List<ApiScope>>();

		services
			.AddIdentityServer(options =>
			{
				// https://docs.duendesoftware.com/identityserver/v6/fundamentals/resources/api_scopes#authorization-based-on-scopes
				options.EmitStaticAudienceClaim = true;
			})
			.AddInMemoryIdentityResources(Config.IdentityResources)
			.AddInMemoryApiScopes(apiScopesFromConfig!)
			.AddInMemoryClients(clients)
			.AddTestUsers(TestUsers.Users);

		return services;
	}

	public static IServiceCollection AddOpenApi(this IServiceCollection services)
	{
		services.AddSwaggerGen();

		return services;
	}

	public static IServiceCollection AddCors(this IServiceCollection services, string policyName)
	{
		services
			.AddCors(
				options =>
				{
					options
						.AddPolicy(
							policyName,
							builder =>
							{
								builder
									.WithOrigins("*")
									.AllowAnyHeader()
									.AllowAnyMethod();
							});
				});

		return services;
	}
}