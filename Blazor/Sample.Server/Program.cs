using Duende.Bff;
using Duende.Bff.Yarp;
using Microsoft.IdentityModel.Logging;
using Sample.Server;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;

var serilogLogger =
    new LoggerConfiguration()
        .MinimumLevel.Information()
        .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
        .MinimumLevel.Override("Microsoft.Hosting.Lifetime", LogEventLevel.Information)
        .MinimumLevel.Override("Microsoft.AspNetCore.Authentication", LogEventLevel.Information)
        .MinimumLevel.Override("IdentityModel", LogEventLevel.Debug)
        .MinimumLevel.Override("Duende.Bff", LogEventLevel.Verbose)
        .Enrich.FromLogContext()
        .WriteTo.Console(
            outputTemplate:
            "[{Timestamp:HH:mm:ss} {Level}] {SourceContext}{NewLine}{Message:lj}{NewLine}{Exception}{NewLine}",
            theme: AnsiConsoleTheme.Code)
        .CreateLogger();

Log.Logger = serilogLogger;

var builder = WebApplication.CreateBuilder(args);
builder.Host.UseSerilog();

IdentityModelEventSource.ShowPII = true;

var configuration = builder.Configuration;
var isHttpsEnabled = configuration.GetValue<bool>("IsHttpsEnabled");
var oidcSettings = new OidcSettings();
configuration.Bind("Oidc", oidcSettings);

var services = builder.Services;

services.AddControllersWithViews();
services.AddRazorPages();

services
    .AddBff()
    .AddRemoteApis();

services
    .AddAuthentication(options =>
    {
        options.DefaultScheme = "cookie";
        options.DefaultChallengeScheme = "oidc";
        options.DefaultSignOutScheme = "oidc";
    })
    .AddCookie("cookie", options =>
    {
        options.Cookie.Name = "__Host-blazor";

        if (isHttpsEnabled)
        {
            options.Cookie.SameSite = SameSiteMode.Strict;
        }
        else
        {
            options.Cookie.SameSite = SameSiteMode.Lax;
            options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
        }
        
    })
    .AddOpenIdConnect("oidc", options =>
    {
        options.Authority = oidcSettings.Authority;
        options.RequireHttpsMetadata = oidcSettings.RequireHttpsMetadata;
        
        options.ClientId = oidcSettings.ClientId;
        options.ClientSecret = oidcSettings.ClientSecret;
        options.ResponseType = oidcSettings.ResponseType;
        options.ResponseMode = oidcSettings.ResponseMode;
        
        options.Scope.Clear();
        foreach (var scopeName in oidcSettings.Scopes)
        {
            options.Scope.Add(scopeName);
        }

        options.MapInboundClaims = oidcSettings.MapInboundClaims;
        options.GetClaimsFromUserInfoEndpoint = oidcSettings.GetClaimsFromUserInfoEndpoint;
        options.SaveTokens = oidcSettings.SaveTokens;
    });

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseWebAssemblyDebugging();
}
else
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

if (isHttpsEnabled)
{
    app.UseHttpsRedirection();
}

app.UseBlazorFrameworkFiles();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseBff();
app.UseAuthorization();

app.MapBffManagementEndpoints();
app.MapRazorPages();

// Local APIs
app.MapControllers()
    .RequireAuthorization()
    .AsBffApiEndpoint();

// Remote APIs
app
    .MapRemoteBffApiEndpoint("/api", "https://sample-remote.free.beeceptor.com/api")
    .RequireAccessToken(TokenType.User);

app.MapFallbackToFile("index.html");

app.Run();
