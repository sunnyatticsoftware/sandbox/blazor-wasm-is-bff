﻿namespace Sample.Server;

public class OidcSettings
{
	public string Authority { get; init; } = string.Empty;
	public bool RequireHttpsMetadata => Authority.Contains("https");
	public string ClientId { get; init; } = string.Empty;
	public string ClientSecret { get; init; } = string.Empty;
	public string ResponseType => "code";
	public string ResponseMode => "query";
	public IEnumerable<string> Scopes { get; init; } = Enumerable.Empty<string>();
	public bool MapInboundClaims => false;
	public bool GetClaimsFromUserInfoEndpoint => true;
	public bool SaveTokens => true;
}